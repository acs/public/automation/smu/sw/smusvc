#ifndef SMUSVC_H
#define SMUSVC_H

#include <QtCore>
#include <QThread>

#include <subcfg.h>
#include <submem.h>

#include <subdaq.h>
#include <subdsp.h>
#include <submon.h>
#include <subnet.h>
#include <suberror.h>

class smuSVC : public QObject
{
    Q_OBJECT

    subCFG *smuCFG;
    subMEM *smuMEM;

    subDAQ  *svcDAQ;
    subDSP  *svcDSP;
    subMON  *svcMON;
    subNET  *svcNET;
    subERROR *svcERROR;

    QThread *DAQ_thread;
    QThread *DSP_thread;
    QThread *NET_thread;

    bool isloaded = false;
    void set_cpu_governor(QString governor);

public:
    smuSVC(QObject *parent);

public slots:
    void start(const QStringList &args);
    void stop();
    void reset();
};

#endif // SMUSVC_H
