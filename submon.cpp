#include "submon.h"

static const char* TAG = "subMON";

subMON::subMON(subCFG *cfg, QObject *parent)
    : QObject(parent)
{
    smuCFG = cfg;
}

void subMON::start()
{
    // Open I2C bus
    fd = open(I2C_DEV, O_RDWR);
    if(fd < 0){
        SMU_LOGE(TAG,"[%1] fail to open i2c bus");
        smuCFG->error.mon = SMU_ERR_DRV_NOT_FOUND;
        return;
    }

    // Associate I2C slave address
    if (ioctl(fd, I2C_SLAVE, SHTC3_ADDRESS)){
        SMU_LOGE(TAG,"[%1] fail to register i2c slave address");
        smuCFG->error.mon = SMU_ERR_DRV_NOT_ASSOC;
        close(fd);
        return;
    }

    // Check SHTC3 present
    i2c_cmd(SHTC3_READ_ID);
    i2c_read(2);
    uint8_t shtc3_id = ((buff[0] << 8 | buff[1]) & SHTC3_MASK_ID) == SHTC3_MASK_ID;
    if (!shtc3_id){
        SMU_LOGE(TAG,"[%1] shtc3 sensor not detected");
        smuCFG->error.mon = SMU_ERR_DRV_NO_SIGNAL;
        close(fd);
        return;
    }

    // MON module started
    SMU_LOGI(TAG,"[%1] module started");
    smuCFG->error.mon = SMU_OK;
}

void subMON::stop()
{
    // Stop I2C driver
    if(!ioctl(fd, I2C_SLAVE, 0)){
        while(close(fd) != 0){}
    } else {
        SMU_LOGE(TAG,"[%1] fail to stop the driver");
        smuCFG->error.mon = SMU_ERR_DRV_NO_STOP;
    }

    // MON module stopped
    SMU_LOGI(TAG,"[%1] module stopped");
    smuCFG->error.mon = SMU_IDLE;
}

void subMON::update()
{
    if ((!smuCFG->error.mon) == SMU_OK)
        return;

    // Start measuring    
    i2c_cmd(SHTC3_READ_TA_NE);

    // Wait conversion
    QEventLoop loop;
    QTimer::singleShot(15, &loop, &QEventLoop::quit);
    loop.exec();

    // Read measurements
    i2c_read(6);

    smuCFG->mon.Ta = ((buff[0] * 256.0 + buff[1]) * 175.0) / 65536.0 - 45.0;
    smuCFG->mon.RH = ((buff[3] * 256.0 + buff[4]) * 100.0) / 65536.0;

    emit updated();
}

void subMON::i2c_cmd(uint16_t reg)
{
    buff[0] = reg >> 8;
	buff[1] = reg;
    if (write(fd, buff, 2) != 2)
        SMU_LOGE(TAG,"[%1] i2c write command fail");
}

void subMON::i2c_read(uint8_t bytes)
{
    if (read(fd, buff, bytes) != bytes)
        SMU_LOGE(TAG,"[%1] i2c read command fail");
}
