#ifndef SUBDSP_H
#define SUBDSP_H

#include <QObject>
#include <QLibrary>
#include <subcfg.h>
#include <submem.h>
//#include <QTime> 

typedef int  (*lib_dsp_init)(subCFG *, subMEM *);
typedef void (*lib_dsp_exit)();
typedef int  (*lib_dsp_proc)();
typedef int  (*lib_dsp_test)();

class subDSP : public QObject
{
    Q_OBJECT

    subCFG *smuCFG;
    subMEM *smuMEM;

    QLibrary     lib_dsp;
    lib_dsp_init dsp_init;
    lib_dsp_exit dsp_exit;
    lib_dsp_proc dsp_proc;
    lib_dsp_test dsp_test;

public:
    subDSP(subCFG *cfg, subMEM *mem, QObject *parent = nullptr);

public slots:
    void start();
    void stop();

    void config();
    void process();

signals:
    void stopped();
    void data();
};

#endif // SUBDSP_H
