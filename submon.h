#ifndef SUBMON_H
#define SUBMON_H

#include <QObject>
#include <QTimer>
#include <subcfg.h>

extern "C" {
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
}

/**
 * @brief  I2C definitions
 *
 * Here are defined the different I2C registers used
 * to communicate with the sensor SHTC3
 */
 
#define I2C_DEV             "/dev/i2c-1"
#define SHTC3_ADDRESS       0x70
#define SHTC3_SLEEP			0xB098
#define SHTC3_WAKEUP		0x3517
#define SHTC3_RESET			0x805D

#define SHTC3_READ_ID       0xEFC8
#define SHTC3_MASK_ID       0x0807      // xxxx 1xxx xx00 0111

#define SHTC3_READ_TA_NE	0x7CA2		// Normal power mode, Clock stretching enabled
#define SHTC3_READ_RH_NE	0x5C24		// Normal power mode, Clock stretching enabled
#define SHTC3_READ_TA_ND	0x7866		// Normal power mode, Clock stretching disabled
#define SHTC3_READ_RH_ND	0x58E0		// Normal power mode, Clock stretching disabled

#define SHTC3_READ_TA_LE	0x6458		// Low power mode, Clock stretching enabled
#define SHTC3_READ_RH_LE	0x44DE		// Low power mode, Clock stretching enabled
#define SHTC3_READ_TA_LD	0x609C		// Low power mode, Clock stretching disabled
#define SHTC3_READ_RH_LD	0x401A		// Low power mode, Clock stretching disabled


class subMON : public QObject
{
    Q_OBJECT

    subCFG *smuCFG;

	int fd;                             // I2C file descriptor
    char buff[6];

    void i2c_cmd(uint16_t reg);
    void i2c_read(uint8_t rbytes);

public:
    subMON(subCFG *cfg, QObject* parent = nullptr);

public slots:
    void start();
    void stop();
    void update();

signals:
    void updated();
};

#endif // SUBMON_H
