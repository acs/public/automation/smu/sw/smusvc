# **smuSVC** <br/> _Daemon for synchronized measurements_

smuSVC is a user-space service for performing synchronized measurements using the SMU hardware-on-top (HAT). Together with [**smuDRV**](smuDRV/README.md), a dedicated kernel-space module for low-latency and high-speed operations, this service allows the implementation of flexible and reconfigurable DAQ applications with high-precision time reference.

## Architecture
The daemon is composed of several sub-modules. The entry point is **main** that creates a detached process for **smuSVC** and register the service with the D-Bus system bus.
This then instantiates all the sub-modules and run them in separate threads:
**subDAQ** handles the communication with smuDRV, by configuring the DAQ engine and transfering data between user- and kernel-space;
**subDSP** handles the processing of raw data via integration of external dynamic libraries;
**subNET** handles the communication to a host via newtwork interface with a specific transport and application protocol;
**subMON** handles the monitoring of environment conditions via on-board temperature and humidity sensors;
**subCAL** handles the computation of proper calibration parameters.

```mermaid
flowchart TD
  subgraph svcsmu[svcSMU]
    svc_init([Init services]) --> svcsmu_init[Spawn threads]
    svcsmu_init --> sub_daq
    svcsmu_init --> sub_dsp
    svcsmu_init --> sub_net
    subgraph sub_net [subNET]
      net_res([Resolve LIB]) --> net_conf[Configure]
      net_sock([Open socket]) ----> net_tcp((TCP/IP))
      net_conf --> net_core[/NET/]
      net_core <--> net_tcp 
    end
    subgraph sub_dsp [subDSP]
      dsp_res([Resolve LIB]) --> dsp_conf[Configure]
      dsp_conf --> dsp_core[/DSP/]
      dsp_core -.->|packets| net_core
    end
    subgraph sub_daq [subDAQ]
      daq_res([Resolve DRV]) --> daq_conf[Configure]
      daq_conf --> daq_core[/DAQ/]
      daq_core -.->|samples| dsp_core
    end
  end
  subgraph drv_smu [drvSMU]
    drv_res([Init DRV]) --> drv_conf[/Char Dev/]
    drv_conf --> drv_daq[/DAQ/]
    drv_daq -.->|frame| daq_core
  end
  subgraph smu_hat [smuHAT]
    gps_irq(GPS) -.->|pps| drv_daq
    adc_irq(ADC) -.->|irq| drv_daq
  end
  drv_conf <-.-> |ioctl| daq_conf
  daq_conf -.- |subCFG| dsp_conf
  dsp_conf -.- |subCFG| net_conf
  net_tcp -.-> |data frame| net_host(Host)
  net_tcp <-.-> |cmd| net_client(Client)
```

## Signals & Slots
The communication between the sub-modules happens via asynchronous signaling.
The service **smuSVC** automatically starts all the sub-modules upon initialization, but if a _[Reset]_ command is received, **subNET** emits a _reset_ signal that is captured by a slot in **smuSVC**, which then starts the reset procedure.
Similarly, if a _[Stop]_ command is received, **subNET** notifies **smuSVC** that will stop all the submodules.
Once the DAQ is started, **subDAQ** registers two callbacks for `SMU_SIG_SYNC` and `SMU_SIG_DATA`.
The former informs the service that a new second-of-century (SOC) is available, whereas the latter that a new buffer frame is ready.
Upon buffer frame notification, **subDAQ** propagates the updated position of the data buffer head to **subDSP**, which starts processing the new data.
When a new reporting frame is ready, **subDSP** timestamps the frame and propagates the updated position of the reporting buffer head to **subNET**.
Finally, a message is formatted according to the specified protocol and sent to the host.

```mermaid
sequenceDiagram
participant GPS
participant DAQ
participant smuDRV
participant smuSVC
participant subDAQ
participant subDSP
participant subNET
participant Host
activate smuSVC
Host->>subNET: [Reset]
activate Host
subNET->>smuSVC:  
activate subDAQ
subDAQ->>smuDRV: SMU_IOC_REG_TASK
subDAQ->>smuDRV: SMU_IOC_START
deactivate subDAQ
smuDRV->>DAQ: Start DAQ
activate DAQ
loop Every PPS
GPS--)smuDRV: SYNC irq
smuDRV--)subDAQ: SMU_SIG_SYNC
activate subDAQ
subDAQ->>subDAQ: Second-Of-Century
end
deactivate subDAQ
loop Every sample
DAQ->>DAQ: Data acquisition
end
loop Every buffer frame
DAQ--)smuDRV: Buffer irq
smuDRV--)subDAQ: SMU_SIG_DATA
activate subDAQ
subDAQ--)subDSP: data_ready(offs)
deactivate subDAQ
activate subDSP
subDSP->>subDSP: Process data
end
loop Every report frame
subDSP--)subNET: frame_ready(offs)
deactivate subDSP
activate subNET
subNET->>subNET: Format frame
subNET->>Host: PACKET
end
deactivate subNET
deactivate Host
activate subDAQ
subDAQ->>smuDRV: SMU_IOC_STOP
deactivate subDAQ
smuDRV->>DAQ: Stop DAQ
deactivate DAQ
deactivate smuSVC
```

## Download
Create the folder **smuSVC** inside the project folder **smu**, then clone the repository inside.
```
mkdir -p ~/smu
cd ~/smu
mkdir smuSVC
git clone <repoSVC> smuSVC
```

Repeat the same procedure for the common library component **smuLIB** (_ignore if already done_).
```
cd ~/smu
mkdir smuLIB
git clone <repoLIB> smuLIB
```

Clone the library [QtDaemon](https://bitbucket.org/kshegunov/qtdaemon/src/master/) used to implement the service abstraction.
```
cd
git clone https://bitbucket.org/kshegunov/qtdaemon.git
```

## Installation
The service was developed in [Qt](https://www.qt.io/), which provides a solid and flexible environment for software development.
To compile smuSVC from command line, the minimal Qt5 packages have to be installed.
```
apt-get install qt5-default qtbase5-dev
```

QtDaemon library also needs to be compiled and installed.
```
cd ~/qtdaemon
qmake
make -j4
make install
```

## Development
To use the graphical development environment QtCreator directly on the Raspberry Pi, additional packages have to be first installed.
```
apt-get install qtcreator qtbase5-examples qtbase5-doc-html
```

QtCreator can then be executed in remote as a backgrounded process over SSH with X11-forwarding.
```
qtcreator &
```
QtCreator uses its own build settings that override those specified in **smuSVC.pro**
Therefore, it is recommended to build the application directly from command line, otherwise the already configured post-build steps will have to be manually redefined in the QtCreator build settings for a seamless integration of the project components.

For additional information and a complete reference on how to setup the development environment for cross-compilation, please refer to the instructions on the [Getting Started with Qt](https://doc.qt.io/qt-5/gettingstarted.html) page.

## Build
Generate the Makefile and build the service.
```
cd ~/smu/smuSVC
qmake
make -j4
```

Upon succesful compilation, the built executable will be available in the project folder **build** with the name **smuSVC**.
```
cd ~/smu/build
ls -l
```

## Usage
To start the newly built SMU daemon as a backgrounded process, we first have to install the service.
The daemon can then be easily controlled as any other service via `systemctl`.
```
cd ~/smu/build
./smuSVC -i
```

This will create a config file for this service in `/etc/init.d`.
Now we need to edit the file and specify the correct runlevel, so to be sure that the service is not started without network support.
```
nano /etc/init.d/smuSVC
```
- [-# Default-Start:     2 3 4 5-]
- [+# Default-Start:     3 5+]

Now we need to reload systemd manager configuration.
The we can start the service and check its status.
Finally,we enable the automatic service start at system boot.<br/>
(_**Note** It is recommended to install the driver smuDRV before running the service_)
```
systemctl daemon-reload
systemctl start smuSVC
systemctl status smuSVC
systemctl enable smuSVC
```

The daemon can also be directly executed in emulated mode from the command line without being installed first, but this is intended to be used only for debug purposes.
```
./smuSVC --fake
```

The application log will be written to **smuSVC.log**.
```
tail -f smuSVC.log
```

## Setting Up Raspberry Pi's NTP server

Here we will activate the Raspberry Pi NTP polling service within a timewindow to maintain the internal clock within an acceptable error margin

```
sudo timedatectl set-ntp true
sudo nano /etc/systemd/timesyncd.conf
```
Now we need to edit the timesyncd.conf polling intervals and IP main NTP server (if you have one), as well as the fallback NTP server (for backup).

```
#set your ntp server IP here by replacing the 10.100.2.1 to your current NTP server IP
NTP=10.100.2.1 
FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
PollIntervalMinSec=16
PollIntervalMaxSec=32
```
(cntrl+x -> y to save the file)

## Copyright

2021, Institute for Automation of Complex Power Systems, EONERC


## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Carlo Guarnieri Calò Carducci, Ph.D.](mailto:cguarnieri@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
