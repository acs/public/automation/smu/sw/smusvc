#include "smusvc.h"
#include <QDaemonApplication>

int main(int argc, char ** argv)
{
    QDaemonApplication app(argc, argv);

    QDaemonApplication::setApplicationName("smuSVC");
    QDaemonApplication::setApplicationDescription("Daemon for SMU measurements");
    QDaemonApplication::setOrganizationDomain("acs.eonerc.rwth-aachen.de");

    smuSVC service(&app);

    QObject::connect(&app, &QDaemonApplication::daemonized, &service, &smuSVC::start);
    QObject::connect(&app, &QDaemonApplication::aboutToQuit, &service, &smuSVC::stop);

    return QDaemonApplication::exec();
}