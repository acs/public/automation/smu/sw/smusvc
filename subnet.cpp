#include "subnet.h"

static const char* TAG = "subNET";

subNET::subNET(subCFG *cfg, subMEM *mem, QObject* parent)
    : QTcpServer(parent)
{
    smuMEM = mem;
    smuCFG = cfg;
}

void subNET::start()
{
    // Open TCP port
    if (!listen(QHostAddress::Any,smuCFG->net.iport)){
        SMU_LOGE(TAG,"[%1] fail to start server");
        smuCFG->error.net = SMU_ERR_NET_PORT_FAIL;
        return;
    }
    // Clear client
    client = nullptr;

    // Route incoming connections
    connect(this,SIGNAL(newConnection()),this,SLOT(addClient()));

    // TCP server started
    SMU_LOGI(TAG,"[%1] server started");
    smuCFG->error.net = SMU_IDLE;
}

void subNET::stop()
{
    // Stop NET module
    if (lib_net.isLoaded())
        net_exit();

    // Stop Thread
    SMU_LOGI(TAG,"[%1] module stopped");
    smuCFG->error.net = SMU_IDLE;
    emit stopped();
}

void subNET::config()
{
    // Unload previously loaded library
    if (lib_net.isLoaded())
        lib_net.unload();

    // Load library
    lib_net.setFileName(QCoreApplication::applicationDirPath() + "/libs/lib" + smuCFG->net.oappl);
    if (!lib_net.load()){
//        qDebug() << lib_net.errorString();
        SMU_LOGE(TAG,"[%1] fail to open library");
        smuCFG->error.net = SMU_ERR_LIB_NOT_FOUND;
        return;
    }

    // Resolve methods
    net_init = reinterpret_cast<lib_net_init>(lib_net.resolve("smuNET_init"));
    net_exit = reinterpret_cast<lib_net_exit>(lib_net.resolve("smuNET_exit"));
    net_proc = reinterpret_cast<lib_net_proc>(lib_net.resolve("smuNET_proc"));
    net_test = reinterpret_cast<lib_net_test>(lib_net.resolve("smuNET_test"));

    if ((!net_init) || (!net_exit) || (!net_proc) || (!net_test)){
        SMU_LOGE(TAG,"[%1] fail to resolve library");
        smuCFG->error.net = SMU_ERR_LIB_NOT_RESOL;
        return;
    }


    // Init NET configuration
    if (net_init(smuCFG, smuMEM, &client)){
        SMU_LOGE(TAG,"[%1] fail to start the module");
        return;
    }


    // NET module configured
    SMU_LOGI(TAG,"[%1] module configured");
    smuCFG->error.net = SMU_OK;
}

void subNET::process()
{
    // Process data in DSP buffer
	if (lib_net.isLoaded())
		net_proc();
}

void subNET::addClient()
{
    client = this->nextPendingConnection();
    connect(client, SIGNAL(disconnected()),client, SLOT(deleteLater()));
    connect(client, SIGNAL(readyRead()),this,SLOT(msgRead()));

    if (smuCFG->net.oappl=="net_dft-ip-server")
        net_init(smuCFG, smuMEM, &client);

    SMU_LOGI(TAG,"[%1] client connected");
}


void subNET::msgRead()
{
    SMU_LOGI(TAG,"[%1] message received");

    QByteArray data = client->readAll();

    buffer = buffer + QString::fromUtf8(data.toStdString().c_str());

    msg *msg_elements;
        msg_elements = new msg;

        msg_elements->sign_main_op='{';
        msg_elements->sign_main_end='}';
        msg_elements->sign_sec_op='[';
        msg_elements->sign_sec_end=']';

        message_processor(&buffer,msg_elements);

        msg_elements->sign_main_op='[';
        msg_elements->sign_main_end=']';
        msg_elements->sign_sec_op='{';
        msg_elements->sign_sec_end='}';

        message_processor(&buffer,msg_elements);
}

void subNET::msgWrite(const QString msg)
{
    if (client->isOpen()){
        QTextStream T(client);
        T << msg;
        client->flush();
    }
    SMU_LOGI(TAG,"[%1] Message sent");
}

void subNET::msgProcess(const QString command)
{
//    qDaemonLog() << QStringLiteral("[%1] Command received: %2.").arg(TAG).arg(command);
    SMU_LOGI(TAG,"[%1] command received");

    if (command.contains("Conf:",Qt::CaseInsensitive)){
        QString config;
        smuCFG->toString(config);
        msgWrite('{'+config+'}');
    }
    if (command.contains("Reset",Qt::CaseInsensitive)){
        emit reset();
    }
    if (command.contains("Debug",Qt::CaseInsensitive)){
//        emit stopDAQ();
//        qDebug() << "subNET: " << "DEBUG ROUTINE! Samples:\t" << smuMEM->dsp_pos << ":" << smuMEM->dsp_dim;
    }
    /*
    if (command.contains("your_specific_command",Qt::CaseInsensitive)){
        //action to be executed
    }
    */
}

void subNET::message_processor(QString *buffer, msg *msg_elements)
{
    bool isLast=false;
    QString message;
    index(buffer, msg_elements);

    int i,j;

    if ((msg_elements->index_main_op==-1)&&(msg_elements->index_sec_op==-1))
        buffer->clear();

    while((msg_elements->index_main_op>-1)&&(isLast==false)){

        while((buffer->at(0)!=msg_elements->sign_main_op)&&(buffer->at(0)!=msg_elements->sign_sec_op)&&(!buffer->isEmpty()))
        {
            buffer->remove(0,1);
        }
        index(buffer, msg_elements);

        i= buffer->indexOf(msg_elements->sign_main_end);

        while((i<msg_elements->index_main_op)&&(i>-1))
        {
            message=buffer->section(msg_elements->sign_main_end,0,0);
            if (((j=message.lastIndexOf(msg_elements->sign_sec_op))>message.lastIndexOf(msg_elements->sign_sec_end)))
            {
                buffer->remove(j,i-j+1);
            }
            else
            {
                if((j=message.lastIndexOf(msg_elements->sign_sec_end))>-1)
                {
                    buffer->remove(j+1,i-j);
                }
                else
                {
                    buffer->remove(0,i+1);
                }
            }

            index(buffer, msg_elements);
            i= buffer->indexOf(msg_elements->sign_main_end);
        }

        if (((msg_elements->index_next_main_op<msg_elements->index_main_end)||(msg_elements->index_main_end==-1))
            &&((msg_elements->index_next_main_op<msg_elements->index_sec_op)||(msg_elements->index_sec_op==-1))
            &&((msg_elements->index_next_main_op<msg_elements->index_sec_end)||(msg_elements->index_sec_end==-1))
            &&(msg_elements->index_next_main_op>-1))
        {
            buffer->remove(msg_elements->index_main_op,msg_elements->index_next_main_op-msg_elements->index_main_op);
            index(buffer, msg_elements);
        }
        else
        {
            if(((msg_elements->index_main_end<msg_elements->index_sec_op)||(msg_elements->index_sec_op==-1))
                &&((msg_elements->index_main_end<msg_elements->index_sec_end)||(msg_elements->index_sec_end==-1))
                &&(msg_elements->index_main_end>-1))
            {
                message=buffer->mid(msg_elements->index_main_op+1,msg_elements->index_main_end-msg_elements->index_main_op-1);
                while((msg_elements->index_main_op>0)&&(buffer->at(msg_elements->index_main_op-1)!=msg_elements->sign_sec_end))
                {
                    buffer->remove(msg_elements->index_main_op-1,1);
                    index(buffer, msg_elements);
                }
                buffer->remove(msg_elements->index_main_op,msg_elements->index_main_end-msg_elements->index_main_op+1);
                if (msg_elements->sign_main_op=='{')
                {
//                    qDaemonLog() << QStringLiteral("[%1] Importing configuration. [%2]").arg(TAG).arg(message);
                    SMU_LOGI(TAG,"[%1] Importing configuration");
                    smuCFG->fromString(message);
                }
                else
                {
                    msgProcess(message);

                }
                index(buffer, msg_elements);
            }
            else
            {
                if(((msg_elements->index_sec_op<msg_elements->index_sec_end)||(msg_elements->index_sec_end==-1))
                    &&(msg_elements->index_sec_op>-1))
                {
                    buffer->remove(msg_elements->index_main_op,msg_elements->index_sec_op-msg_elements->index_main_op);
                    index(buffer, msg_elements);
                }
                else
                {
                    if(msg_elements->index_sec_end>-1)
                    {
                        buffer->remove(msg_elements->index_main_op,msg_elements->index_sec_end-msg_elements->index_main_op+1);
                        index(buffer, msg_elements);
                    }
                    else
                    {
                        isLast=true;
                    }
                }
            }
        }
    }
    if (((!buffer->contains(msg_elements->sign_main_op))&&(!buffer->contains(msg_elements->sign_sec_op))))
        buffer->clear();
}

void subNET::index(QString *text, msg *elements)
{
    elements->index_main_op=text->indexOf(elements->sign_main_op);
    if (elements->index_main_op>-1)
    {
        elements->index_next_main_op=text->indexOf(elements->sign_main_op,elements->index_main_op+1);
        elements->index_main_end=text->indexOf(elements->sign_main_end,elements->index_main_op);
        elements->index_sec_op=text->indexOf(elements->sign_sec_op,elements->index_main_op);
        elements->index_sec_end=text->indexOf(elements->sign_sec_end,elements->index_main_op);
    }
}
