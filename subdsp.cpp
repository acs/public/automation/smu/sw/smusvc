#include "subdsp.h"

static const char* TAG = "subDSP";
static uint32_t count;
static bool start_pos;

subDSP::subDSP(subCFG *cfg, subMEM *mem, QObject *parent)
    : QObject(parent)
{
    smuCFG = cfg;
    smuMEM = mem;
}

void subDSP::start()
{
	// DSP module started
    SMU_LOGI(TAG,"[%1] module started");
    smuCFG->error.dsp = SMU_IDLE;
    start_pos = false;
}

void subDSP::stop()
{

    // Stop DSP module
    if(lib_dsp.isLoaded())
        dsp_exit();    

    // Stop Thread
    SMU_LOGI(TAG,"[%1] module stopped");
    smuCFG->error.dsp = SMU_IDLE;

    emit stopped();

}

void subDSP::config()
{
    // Unload previously loaded library
    if (lib_dsp.isLoaded())
        lib_dsp.unload();

    // Update DSP memory configuration
    smuMEM->dsp_pos = 0;
    smuMEM->dsp_inc = 0;
    smuMEM->dsp_dim = 0;

    // Load library
    lib_dsp.setFileName(QCoreApplication::applicationDirPath() + "/libs/lib" + smuCFG->dsp.core);
    if (!lib_dsp.load()){
//        qDebug() << lib_dsp.errorString();
        SMU_LOGE(TAG,"[%1] fail to open library");
        smuCFG->error.dsp = SMU_ERR_LIB_NOT_FOUND;
        return;
    }

    // Resolve methods
    dsp_init = reinterpret_cast<lib_dsp_init>(lib_dsp.resolve("smuDSP_init"));
    dsp_exit = reinterpret_cast<lib_dsp_exit>(lib_dsp.resolve("smuDSP_exit"));
    dsp_proc = reinterpret_cast<lib_dsp_proc>(lib_dsp.resolve("smuDSP_proc"));
    dsp_test = reinterpret_cast<lib_dsp_test>(lib_dsp.resolve("smuDSP_test"));

    if ((!dsp_init) || (!dsp_exit) || (!dsp_proc) || (!dsp_test)){
        SMU_LOGE(TAG,"[%1] fail to resolve library");
        smuCFG->error.dsp = SMU_ERR_LIB_NOT_RESOL;
        return;
    }

    // Init DSP configuration
    if (dsp_init(smuCFG, smuMEM)){
        SMU_LOGE(TAG,"[%1] fail to start the module");
        return;
    }

    // DSP module configured
    SMU_LOGI(TAG,"[%1] module configured");
    smuCFG->error.dsp = SMU_OK;
}

void subDSP::process()
{
    // Process data in DAQ buffer
    if (!start_pos)
        {
            if (smuMEM->daq_pos==0)
            {
                start_pos = true;
            }
        }
    if (lib_dsp.isLoaded()&&start_pos)
    {
		for (count = 0; count < (smuCFG->daq.rate*1000/smuCFG->daq.buff); count++, smuMEM->daq_pos++)
        {
			if(dsp_proc()>=0)
            {
				emit data();
            }
        }
    }
}