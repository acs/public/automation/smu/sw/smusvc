#ifndef SUBNET_H
#define SUBNET_H

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QCoreApplication>
#include <QLibrary>
#include <subcfg.h>
#include <submem.h>

typedef int  (*lib_net_init)(subCFG *, subMEM *, QTcpSocket **);
typedef void (*lib_net_exit)();
typedef int  (*lib_net_proc)();
typedef int  (*lib_net_test)();

struct msg
{
    int index_main_op;
    int index_next_main_op;
    int index_main_end;
    int index_sec_op;
    int index_sec_end;
    QChar sign_main_op;
    QChar sign_main_end;
    QChar sign_sec_op;
    QChar sign_sec_end;
};

class subNET : public QTcpServer
{
    Q_OBJECT

    subCFG *smuCFG;
    subMEM *smuMEM;
    QTcpSocket *client;

    QLibrary     lib_net;
    lib_net_init net_init;
    lib_net_exit net_exit;
    lib_net_proc net_proc;
    lib_net_test net_test;

public:
    subNET(subCFG *cfg, subMEM *mem, QObject* parent = nullptr);

public slots:
    void start();
    void stop();

    void config();
    void process();

    void addClient();

    void msgRead();
    void msgWrite(const QString msg);
    void msgProcess(const QString command);

signals:
    void stopped();
    void reset();

private:
    QString buffer, conf, command;  //stores message, command and configuration fragments

    void index(QString *text, msg *elements);
    void message_processor(QString *buffer, msg *msg_elements);
};

#endif // SUBNET_H
