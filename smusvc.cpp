#include "smusvc.h"

static const char* TAG = "smuSVC";

smuSVC::smuSVC(QObject * parent)
    : QObject(parent)
{
    QThread::currentThread()->setPriority(QThread::NormalPriority);
}

void smuSVC::start(const QStringList &args)
{
    smuCFG = new subCFG();
    smuMEM = new subMEM(smuCFG);
    svcDAQ = new subDAQ(smuCFG,smuMEM);
    svcDSP = new subDSP(smuCFG,smuMEM);
    svcNET = new subNET(smuCFG,smuMEM);
    svcMON = new subMON(smuCFG);

    /************************************************* Threads ***********************************************/
    DAQ_thread = new QThread(this);
    svcDAQ->moveToThread(DAQ_thread);
    QObject::connect(DAQ_thread, SIGNAL(started()), svcDAQ,     SLOT(start()));
    QObject::connect(svcDAQ,     SIGNAL(stopped()), DAQ_thread, SLOT(quit()));
    QObject::connect(svcDAQ,     SIGNAL(stopped()), svcDAQ,     SLOT(deleteLater()));
    QObject::connect(DAQ_thread, SIGNAL(finished()),DAQ_thread, SLOT(deleteLater()));

    DSP_thread = new QThread(this);
    svcDSP->moveToThread(DSP_thread);
    QObject::connect(DSP_thread, SIGNAL(started()), svcDSP,     SLOT(start()));
    QObject::connect(svcDSP,     SIGNAL(stopped()), DSP_thread, SLOT(quit()));
    QObject::connect(svcDSP,     SIGNAL(stopped()), svcDSP,     SLOT(deleteLater()));
    QObject::connect(DSP_thread, SIGNAL(finished()),DSP_thread, SLOT(deleteLater()));

    NET_thread = new QThread(this);
    svcNET->moveToThread(NET_thread);
    QObject::connect(NET_thread, SIGNAL(started()), svcNET,     SLOT(start()));
    QObject::connect(svcNET,     SIGNAL(stopped()), NET_thread, SLOT(quit()));
    QObject::connect(svcNET,     SIGNAL(stopped()), svcNET,     SLOT(deleteLater()));
    QObject::connect(NET_thread, SIGNAL(finished()),NET_thread, SLOT(deleteLater()));

    /************************************************* Logic ***********************************************/
    QObject::connect(svcNET, SIGNAL(reset()),this,   SLOT(reset()));

    QObject::connect(svcDAQ, SIGNAL(data()), svcDSP, SLOT(process()));                  /*!< Send raw data for processing */
    QObject::connect(svcDSP, SIGNAL(data()), svcNET, SLOT(process()));                  /*!< Send processed data to the host */
    QObject::connect(svcDAQ, SIGNAL(sync()), svcMON, SLOT(update()));                   /*!< Read sensors data */

    // SMU daemon installed
    smuCFG->error.svc = smuCFG->error.daq = smuCFG->error.dsp= smuCFG->error.net= SMU_IDLE;
    // qDaemonLog(args.join(","),QDaemonLog::NoticeEntry);

    // Init configuration
    smuCFG->load();
	
    // Start submodules and threads
    DAQ_thread->start(QThread::HighestPriority);
    DSP_thread->start(QThread::HighPriority);
    NET_thread->start(QThread::NormalPriority);
    svcMON->start();
	
    while (!DAQ_thread->isRunning() || !DSP_thread->isRunning() || !NET_thread->isRunning()) ;
    QObject::connect(smuCFG, SIGNAL(updated()), svcDAQ, SLOT(config()));
    QObject::connect(smuCFG, SIGNAL(updated()), svcDSP, SLOT(config()));
    QObject::connect(smuCFG, SIGNAL(updated()), svcNET, SLOT(config()));

    // Update submodules configuration
    emit smuCFG->updated();

    SMU_LOGI(TAG,"[%1] daemon started");
    smuCFG->error.svc = SMU_OK;
    isloaded = true;
    set_cpu_governor("performance");
}

void smuSVC::stop()
{
    if (!isloaded)
        return;

    // Stop submodules and threads
    svcDAQ->stop();
    while(DAQ_thread->isRunning()){}

    svcDSP->stop();
    while(DSP_thread->isRunning()){}

    svcNET->stop();
    while(NET_thread->isRunning()){}

    svcMON->stop();

    // Save configuration
    smuCFG->save();
	
    SMU_LOGI(TAG,"[%1] daemon stopped");
    smuCFG->error.svc = SMU_IDLE;
    isloaded = false;
    set_cpu_governor("ondemand");
}

void smuSVC::reset()
{
    // Stop daemon
    stop();

    // Start daemon
    // start();
}

void smuSVC::set_cpu_governor(QString governor)
{
    QProcess process1, process2;
    process1.setStandardOutputProcess(&process2);

    process1.start(QString("echo %1").arg(governor));
    process2.start("sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
    process2.setProcessChannelMode(QProcess::ForwardedChannels);

    process2.waitForFinished();
    process1.waitForFinished();
}
