#-----------------------------------------------------------------
#
#   Carlo Guarnieri Calò Carducci
#
#-----------------------------------------------------------------

QT       = core daemon network widgets

TARGET   = smuSVC
TEMPLATE = app
CONFIG  += c++11 console qt
CONFIG  -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
SOURCES +=  main.cpp \
            smusvc.cpp \
            subdaq.cpp \
            subdsp.cpp \
            submon.cpp \
            subnet.cpp \
            suberror.cpp

HEADERS +=  smusvc.h \
            subdaq.h \
            subdsp.h \
            submon.h \
            subnet.h \
            suberror.h

include(../smuLIB/smuLIB.pri)

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_LFLAGS_RELEASE -= -O1

BUILDDIR = ./build
DESTDIR = ../build
OBJECTS_DIR = $$BUILDDIR
MOC_DIR = $$BUILDDIR
RCC_DIR = $$BUILDDIR
