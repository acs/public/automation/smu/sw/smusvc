#ifndef SUBERROR_H
#define SUBERROR_H

#include <QObject>
#include <QMutex>
#include <QtMath>

struct error{
    double gain;
    double gain_drift;
    double freq;
    double freq_drift;
    double filter;
    double filter_drift;
    double time_delay;
    double phase;
    double amplitude;
};

class subERROR : public QObject
{
    Q_OBJECT
public:
    subERROR(QObject *parent = 0);
    double read (QString field_id);

signals:

public slots:
    void update (double temp, double hum);

private:
    QMutex mutex;
    error deviation;

};

#endif // SUBERROR_H
