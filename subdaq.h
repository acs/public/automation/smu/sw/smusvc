// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SUBDAQ_H
#define SUBDAQ_H

#include <QObject>
#include <subcfg.h>
#include <submem.h>

extern "C" {

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
}

/**
 * @brief  IOCTL definitions
 *
 * Here are defined the different IOCTL commands used
 * to communicate from the userspace to the kernel module.
 */
#define SMU_DEV             "/dev/smu"
#define SMU_IOC_MAGIC       'k'
#define SMU_IOC_REG_TASK    _IO (SMU_IOC_MAGIC, 0)
#define SMU_IOC_RESET       _IO (SMU_IOC_MAGIC, 1)
#define SMU_IOC_START       _IO (SMU_IOC_MAGIC, 2)
#define SMU_IOC_STOP        _IO (SMU_IOC_MAGIC, 3)
#define SMU_IOC_GET_TIME    _IOR(SMU_IOC_MAGIC, 4, timespec64_t*)
#define SMU_IOC_SET_CONF    _IOW(SMU_IOC_MAGIC, 5, smu_conf_t*)
#define SMU_IOC_GET_CONF    _IOR(SMU_IOC_MAGIC, 6, smu_conf_t*)


/**
 * @brief  Signal definitions
 *
 * Here are defined the different values used to send
 * signals to the userspace
 */
#define SMU_SIG_SYNC        41
#define SMU_SIG_DATA        42


class subDAQ : public QObject
{
    Q_OBJECT

    subCFG *smuCFG;
    subMEM *smuMEM;

    int fd; // SMU file descriptor
    static void sync_event_handler(int n, siginfo_t *info, void *ucontext);
    static void data_event_handler(int n, siginfo_t *info, void *ucontext);

public:
    subDAQ(subCFG *cfg, subMEM *mem, QObject* parent = nullptr);

public slots:
    void start();
    void stop();

    void config();

signals:
    void stopped();
    void data();
    void sync();
};

#endif // SUBDAQ_H
