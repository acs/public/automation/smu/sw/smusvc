#include "subdaq.h"

static const char* TAG = "subDAQ";
static struct sigaction act;
static subDAQ  *This;

subDAQ::subDAQ(subCFG *cfg, subMEM *mem, QObject *parent)
    : QObject(parent)
{
    This = this;
    smuMEM = mem;
    smuCFG = cfg;
}

void subDAQ::sync_event_handler(int, siginfo_t *info, void*)
{
    // Get timestamp rounded to second
    ioctl(This->fd, SMU_IOC_GET_TIME,&This->smuMEM->time);
    if (This->smuMEM->time.tv_nsec>700000000)
        This->smuMEM->tref = QDateTime::fromSecsSinceEpoch(This->smuMEM->time.tv_sec+1);
    else
        This->smuMEM->tref = QDateTime::fromSecsSinceEpoch(This->smuMEM->time.tv_sec);
    emit This->sync();
}

void subDAQ::data_event_handler(int, siginfo_t *info, void*)
{
    // Update multi-sample buffer position
    //This->smuMEM->daq_pos = (info->si_value.sival_int >> 4);// + ADC_TX_RATE;  // daq_pos = (drv_pos / 16) + ADC_TX_RATE
    This->smuMEM->daq_pos = (info->si_value.sival_int);
    emit This->data();
}

void subDAQ::start()
{
    // Open DAQ driver
    fd = open(SMU_DEV, O_RDWR);
    if(fd < 0){
        SMU_LOGE(TAG,"[%1] fail to open the device driver");
        smuCFG->error.daq = SMU_ERR_DRV_NOT_FOUND;
        return;
    }

    // Associate kernel-to-user Task ID
    if (ioctl(fd, SMU_IOC_REG_TASK, 0)){
        SMU_LOGE(TAG,"[%1] fail to register the driver");
        smuCFG->error.daq = SMU_ERR_DRV_NOT_ASSOC;
        close(fd);
        return;
    }

    // Map kernel-to-user shared memory
    void *sh_mem = mmap(nullptr, MMAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (sh_mem == MAP_FAILED){
        SMU_LOGE(TAG,"[%1] fail to map driver memory");
        smuCFG->error.daq = SMU_ERR_DRV_MMAP_FAIL;
        return;
    }
    smuMEM->daq_data = (uint8_t*)sh_mem;

    // Install PPS signal handler
    sigemptyset(&act.sa_mask);
    act.sa_flags = (SA_SIGINFO | SA_RESTART);
    act.sa_sigaction = sync_event_handler;
    if (sigaction(SMU_SIG_SYNC, &act, nullptr)){
        SMU_LOGE(TAG,"[%1] fail to install PPS handler");
        smuCFG->error.daq = SMU_ERR_DRV_NO_SIGNAL;
        return;
    }

    // Install ADC signal handler
    sigemptyset(&act.sa_mask);
    act.sa_flags = (SA_SIGINFO | SA_RESTART);
    act.sa_sigaction = data_event_handler;
    if (sigaction(SMU_SIG_DATA, &act, nullptr)){
        SMU_LOGE(TAG,"[%1] fail to install ADC handler");
        smuCFG->error.daq = SMU_ERR_DRV_NO_SIGNAL;
        return;
    }

    // DAQ module started
    SMU_LOGI(TAG,"[%1] module started");
    smuCFG->error.daq = SMU_OK;
}

void subDAQ::stop()
{
    // Stop DAQ driver
    if(!ioctl(fd, SMU_IOC_STOP, 0)){
        // Unmap kernel-to-user shared memory
        munmap(smuMEM->daq_data, MMAP_SIZE);
        while(close(fd) != 0){}
    } else {
        SMU_LOGE(TAG,"[%1] fail to stop the driver");
        smuCFG->error.daq = SMU_ERR_DRV_NO_STOP;
    }

    // Stop Thread
    SMU_LOGI(TAG,"[%1] module stopped");
    smuCFG->error.daq = SMU_IDLE;
    emit stopped();
}

void subDAQ::config()
{
    // Check status
    if (smuCFG->error.daq){
        SMU_LOGW(TAG,"[%1] cannot configure the module");
        return;
    }
	
    // Update DAQ memory configuration
    smuMEM->daq_pos = 0;
    smuMEM->daq_inc = sizeof(smu_mcsc_t);
    smuMEM->daq_dim = smuCFG->daq.rate;


    // Stop DAQ driver
    if(ioctl(fd, SMU_IOC_STOP, 0)){
        SMU_LOGE(TAG,"[%1] fail to stop the driver");
        smuCFG->error.daq = SMU_ERR_DRV_NO_STOP;
    }

    // Configure DAQ driver
    if (write(fd, &smuCFG->daq, sizeof(smuCFG->daq)) < 0){
        smuCFG->error.daq = SMU_ERR_DRV_NOT_CONF;
        SMU_LOGE(TAG,"[%1] fail to configure the driver");
        return;
    }

    // Start DAQ driver
    if(ioctl(fd, SMU_IOC_START, 0)){
        SMU_LOGE(TAG,"[%1] fail to start the driver");
        smuCFG->error.daq = SMU_ERR_DRV_NO_START;
        return;
    }
	
    // DAQ module configured
    SMU_LOGI(TAG,"[%1] module configured");
}
